import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FriendsProvider with ChangeNotifier {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  List<String> _friends = [];

  List<String> get friends => _friends;

  void fetchFriends() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      DocumentSnapshot userDoc =
          await _firestore.collection('Users').doc(currentUser.uid).get();
      if (userDoc.exists && userDoc.data() is Map<String, dynamic>) {
        _friends = List.from(
            (userDoc.data() as Map<String, dynamic>)['friends'] ?? []);
        notifyListeners();
      }
    }
  }

  void addFriend(String friendUid) async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      _friends.add(friendUid);
      notifyListeners();

      await _firestore.collection('Users').doc(currentUser.uid).update({
        'friends': FieldValue.arrayUnion([friendUid])
      });

      await _firestore.collection('Users').doc(friendUid).update({
        'friends': FieldValue.arrayUnion([currentUser.uid])
      });
    }
  }

  void removeFriend(String friendUid) async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      _friends.remove(friendUid);
      notifyListeners();

      await _firestore.collection('Users').doc(currentUser.uid).update({
        'friends': FieldValue.arrayRemove([friendUid])
      });

      await _firestore.collection('Users').doc(friendUid).update({
        'friends': FieldValue.arrayRemove([currentUser.uid])
      });
    }
  }

  bool isFriend(String userId) {
    return _friends.contains(userId);
  }
}

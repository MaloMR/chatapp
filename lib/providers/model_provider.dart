import 'package:flutter/material.dart';

class ModelProvider with ChangeNotifier {
  static final ModelProvider _instance = ModelProvider._internal();
  factory ModelProvider() => _instance;
  ModelProvider._internal();

  String _selectedModel = '3.5-turbo';
  
  String get selectedModel => _selectedModel;

  void selectModel(String model) {
    _selectedModel = model;
    notifyListeners();
  }
}

import 'package:chatapp/providers/username_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  User? getCurrentUser() {
    return _auth.currentUser;
  }

  Future<UserCredential> signInWithEmailPassword(String email, password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      return userCredential;
    } on FirebaseAuthException catch (ex) {
      throw Exception(ex.code);
    }
  }

  Future<UserCredential> signUpWithEmailPassword(String email, password) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      _firestore.collection("Users").doc(userCredential.user!.uid).set(
          {'uid': userCredential.user!.uid, 'email': email, 'username': email, 'friends': []});

      return userCredential;
    } on FirebaseAuthException catch (ex) {
      throw Exception(ex.code);
    }
  }

  Future<void> changeUsername(String username, BuildContext context) async {
    final user = _auth.currentUser;
    await _firestore.collection("Users").doc(user!.uid).update({'username': username});
    Provider.of<UsernameProvider>(context, listen: false).setUsername(username);
  }

  Future<void> signOut() async {
    return await _auth.signOut();
  }

  Future<void> loadInitialUsername(BuildContext context) async {
    if (_auth.currentUser != null) {
      try {
        DocumentSnapshot userDoc = await _firestore.collection("Users").doc(_auth.currentUser!.uid).get();
        var userData = userDoc.data() as Map<String, dynamic>?;
        if (userData != null && userData.containsKey('username')) {
          String username = userData['username'];
          Provider.of<UsernameProvider>(context, listen: false).setUsername(username);
        }
      } catch (e) {
        print("Failed to fetch username at startup: $e");
      }
    }
  }
}

import 'package:chatapp/env.dart';
import 'package:chatapp/models/message.dart';
import 'package:chatapp/providers/model_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class GptService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Stream<List<Map<String, dynamic>>> getUsersStream() {
    return _firestore.collection("Users").snapshots().map((snapshot) {
      return snapshot.docs.map((doc) {
        final user = doc.data();
        return user;
      }).toList();
    });
  }

  Future<void> sendMessage(String message) async {
    const String receiverID = "GPT";
    final String currentUserID = _auth.currentUser!.uid;
    final String currentUserEmail = _auth.currentUser!.email!;
    final Timestamp timestamp = Timestamp.now();

    Message newMessage = Message(
        senderID: currentUserID,
        senderEmail: currentUserEmail,
        receiverID: receiverID,
        message: message,
        timestamp: timestamp);

    List<String> ids = [currentUserID, receiverID];
    ids.sort();
    String chatRoomID = ids.join('_');

    await _firestore
        .collection("chat_rooms")
        .doc(chatRoomID)
        .collection("messages")
        .add(newMessage.toMap());

    sendGptMessage(currentUserID, message);
  }

  Future<void> sendGptMessage(String receiverID, receivedMessage) async {
    const String currentUserID = "GPT";
    const String currentUserEmail = "GPT";
    final Timestamp timestamp = Timestamp.now();

    final answerMessage = await apiCall(receivedMessage);

    Message newMessage = Message(
        senderID: currentUserID,
        senderEmail: currentUserEmail,
        receiverID: receiverID,
        message: answerMessage,
        timestamp: timestamp);

    List<String> ids = [currentUserID, receiverID];
    ids.sort();
    String chatRoomID = ids.join('_');

    await _firestore
        .collection("chat_rooms")
        .doc(chatRoomID)
        .collection("messages")
        .add(newMessage.toMap());
  }

  Future<String> apiCall(String message) async {
    const apiUrl = 'https://api.openai.com/v1/chat/completions';
    final apiKey = Env.apiKey;
    final model = "gpt-${ModelProvider().selectedModel}";

    try {
      var response = await http.post(
        Uri.parse(apiUrl),
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': 'Bearer $apiKey'
        },
        body: jsonEncode({
          'model': model,
          'messages': [
            {
              'role': 'user',
              'content': message,
            },
            {
              'role': 'system',
              'content':
                  'Make short and concise answers. Do not use long sentences.',
            },
          ],
          'max_tokens': 150,
          'temperature': 0.9,
        }),
      );

      if (response.statusCode == 200) {
        var responseData = jsonDecode(utf8.decode(response.bodyBytes));
        var result = responseData['choices'][0]['message']['content'];
        return result;
      } else {
        throw Exception('Failed to load data: ${response.statusCode}');
      }
    } catch (e) {
      print('Caught error: $e');
      rethrow;
    }
  }

  Stream<QuerySnapshot> getMessages(String userID) {
    List<String> ids = [userID, "GPT"];
    ids.sort();
    String chatRoomID = ids.join('_');

    return _firestore
        .collection("chat_rooms")
        .doc(chatRoomID)
        .collection("messages")
        .orderBy("timestamp", descending: false)
        .snapshots();
  }
}

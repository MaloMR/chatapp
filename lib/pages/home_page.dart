import 'package:chatapp/components/user_tile.dart';
import 'package:chatapp/pages/chat_page.dart';
import 'package:chatapp/components/my_drawer.dart';
import 'package:chatapp/pages/search_page.dart';
import 'package:chatapp/providers/username_provider.dart';
import 'package:chatapp/providers/friends_provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    Provider.of<FriendsProvider>(context, listen: false).fetchFriends();

    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        title: Consumer<UsernameProvider>(
          builder: (context, usernameProvider, child) {
            return Text(usernameProvider.username);
          },
        ),
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey,
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: IconButton(
                icon: const Icon(
                  Icons.search,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SearchPage()));
                }),
          )
        ],
      ),
      drawer: const MyDrawer(),
      body: _buildFriendsList(context),
    );
  }

  Widget _buildFriendsList(BuildContext context) {
    return Consumer<FriendsProvider>(
      builder: (context, friendsProvider, child) {
        if (friendsProvider.friends.isEmpty) {
          return const Center(child: Text("No friends added yet."));
        }
        return ListView.builder(
          itemCount: friendsProvider.friends.length,
          itemBuilder: (context, index) {
            String friendUid = friendsProvider.friends[index];
            return FutureBuilder<DocumentSnapshot>(
              future: FirebaseFirestore.instance.collection('Users').doc(friendUid).get(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                }
                if (snapshot.hasError) {
                  return const Text("Error loading friend data.");
                }
                var userData = snapshot.data!.data() as Map<String, dynamic>;
                return UserTile(
                  text: userData['username'],
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChatPage(
                          receiverUsername: userData["username"],
                          receiverID: userData["uid"],
                        )
                      )
                    );
                  }
                );
              },
            );
          },
        );
      }
    );
  }
}

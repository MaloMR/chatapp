import 'dart:async';
import 'package:chatapp/providers/friends_provider.dart';
import 'package:chatapp/services/auth/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final TextEditingController _searchController = TextEditingController();
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Timer? _debounce;
  Stream<QuerySnapshot>? _userStream;

  @override
  void initState() {
    super.initState();
    Provider.of<FriendsProvider>(context, listen: false).fetchFriends();
  }

  void _searchByEmail(String input) {
    final currentUser = AuthService().getCurrentUser();
    if (currentUser == null) return;

    if (_debounce?.isActive ?? false) _debounce!.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      if (input.isEmpty) {
        setState(() {
          _userStream = null;
        });
      } else {
        setState(() {
          _userStream = _firestore
              .collection('Users')
              .where('email', isGreaterThanOrEqualTo: input)
              .where('email', isLessThan: '${input}z')
              .where(FieldPath.documentId, isNotEqualTo: currentUser.uid)
              .snapshots();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Add friends"),
          backgroundColor: Colors.transparent,
          foregroundColor: Colors.grey,
          elevation: 0),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              autofocus: true,
              controller: _searchController,
              decoration: InputDecoration(
                hintText: "Search for friends by email",
                suffixIcon: IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () =>
                        _searchByEmail(_searchController.text.trim())),
              ),
              onChanged: _searchByEmail,
            ),
          ),
          Expanded(
            child: _userStream == null
                ? const Center(child: Text("Enter an email to start searching"))
                : StreamBuilder<QuerySnapshot>(
                    stream: _userStream,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      }
                      if (snapshot.hasError) {
                        return Center(child: Text("Error: ${snapshot.error}"));
                      }
                      if (snapshot.data!.docs.isEmpty) {
                        return const Center(child: Text("No users found."));
                      }

                      return ListView(
                        children: snapshot.data!.docs.map((doc) {
                          var user = doc.data() as Map<String, dynamic>;
                          bool isAlreadyFriend = Provider.of<FriendsProvider>(
                                  context,
                                  listen: false)
                              .isFriend(user['uid']);
                          return ListTile(
                            title: Text(user['email']),
                            subtitle: Text(isAlreadyFriend
                                ? "Already a friend"
                                : "Click to add"),
                            onTap: () {
                              if (!isAlreadyFriend) {
                                Provider.of<FriendsProvider>(context,
                                        listen: false)
                                    .addFriend(user['uid']);
                              }
                            },
                          );
                        }).toList(),
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }
}

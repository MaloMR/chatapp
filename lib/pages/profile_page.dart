import 'package:chatapp/components/my_button.dart';
import 'package:chatapp/components/my_textfield.dart';
import 'package:chatapp/providers/username_provider.dart';
import 'package:chatapp/services/auth/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfilePage extends StatelessWidget {
  ProfilePage({super.key});

  final TextEditingController _usernameController = TextEditingController();

  void save(BuildContext context) {
    final newUsername = _usernameController.text;
    AuthService().changeUsername(newUsername, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        title: const Text("Profile"),
        backgroundColor: Colors.transparent,
        foregroundColor: Colors.grey,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.person,
              size: 80,
              color: Theme.of(context).colorScheme.primary,
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Current username: ",
                  style:
                      TextStyle(color: Theme.of(context).colorScheme.primary),
                ),
                Consumer<UsernameProvider>(
                  builder: (context, usernameProvider, child) {
                    return Text(usernameProvider.username);
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            MyTextField(
              hintText: "New username",
              obscureText: false,
              controller: _usernameController,
            ),
            const SizedBox(
              height: 25,
            ),
            MyButton(
              text: "Save",
              onTap: () => save(context),
            ),
            const SizedBox(
              height: 25,
            ),
          ],
        ),
      ),
    );
  }
}
